<?php
header('Access-Control-Allow-Origin: *');
sleep(0.8);

$data = array();

$action = $_POST['action']; // метод запроса {{string}}


$in_cart = 5;
$in_favorite = 18;
$discount = rand(5, 15) * 100;

function randomDate($start_date, $end_date){
	$min = strtotime($start_date);
	$max = strtotime($end_date) ? strtotime($end_date) : $end_date;

	$val = rand($min, $max);

	return date('Y-m-d', $val);
}

function getRandomCollection(){

	$collection = '';

	switch (rand(0, 3)){
		case 0:
			$collection = 'CRYSTALLIT';
			break;
		case 1:
			$collection = 'DANKE';
			break;
		case 2:
			$collection = 'ESTERA';
			break;
		case 3:
			$collection = 'MOELLER';
			break;
	}

	return $collection;
}

function getRandomMaterial(){

	$material = '';

	switch (rand(0, 1)){
		case 0:
			$material = 'матовый';
			break;
		case 1:
			$material = 'глянцевый';
			break;
	}

	return $material;
}

function getRandomColor(){

	$color = '';

	switch (rand(0, 21)){
		case 0:
			$color = "Глория";
			break;
		case 1:
			$color = "Метелик";
			break;
		case 2:
			$color = "Вилья";
			break;
		case 3:
			$color = "Сауле";
			break;
		case 4:
			$color = "Антрацит";
			break;
		case 5:
			$color = "Серебристый ясень";
			break;
		case 6:
			$color = "Пазл";
			break;
		case 7:
			$color = "Серо-бежевый мрамор";
			break;
		case 8:
			$color = "Оранжевый";
			break;
		case 9:
			$color = "Бежевый мрамор";
			break;
		case 10:
			$color = "Серый мрамор";
			break;
		case 11:
			$color = "Коричневый дуб";
			break;
		case 12:
			$color = "Венге";
			break;
		case 13:
			$color = "Белое дерево";
			break;
		case 14:
			$color = "Горная лиственница";
			break;
		case 15:
			$color = "Дуб янтарный";
			break;
		case 16:
			$color = "Орех";
			break;
		case 17:
			$color = "Золотой дуб";
			break;
		case 18:
			$color = "Дуб натуральный";
			break;
		case 19:
			$color = "Белый";
			break;
		case 20:
			$color = "Махагон";
			break;
		case 21:
			$color = "Розовый оникс";
			break;
	}

	return $color;
}

switch ($action) {

	case 'get_common_info':

		$data['status'] = true;

		$data['info'] = array(
			"in_cart" => $in_cart,
			"in_favourite" => $in_favorite,
		);

		break;

	case 'get_product_list':

		$data['status'] = true;

		$data['list'] = array();

		for ($i = 1; $i <= 30; $i++) {

			array_push($data['list'], array(
				"id" => $i,
				"collection" => getRandomCollection(),
				"name" => getRandomColor() . " " . getRandomMaterial(),
				"image" => "http://sill.maestros.ru/img/prod.png",
				"price" => rand(1, 10) * 100,
				"dimension" => "м. пог",
				"in_favourite" => rand(0, 3) == 1,
				"in_cart" => rand(0, 4) == 1,
			));

		}

		break;

	case 'get_product_item':

		$data['status'] = true;

		$data['item'] = array(
			"id" => $_POST['id'],
			"collection" => getRandomCollection(),
			"name" => getRandomColor() . " " . getRandomMaterial(),
			"images" => array(
				'http://placekitten.com/g/1200/900',
				'http://placekitten.com/g/1100/500',
				'http://placekitten.com/g/800/800',
				'http://placekitten.com/g/900/800',
				'http://placekitten.com/g/1300/600',
				'http://placekitten.com/g/700/800',
				'http://placekitten.com/g/500/1000',
			),
			"price" => rand(1, 10) * 100,
			"dimension" => "м. пог",
			"in_favourite" => rand(0, 3) === 1,
		);

		$data['form'] = array(
			"lgth" => array(		// длина
				"value" => '',
				"ones" => 100,		// целая единица длины (длина по которой расчитывается цена на единицу изделия - 100 cм. = 1 единица = 700 руб.)
				"maxValue" => 600
			),
			"surface" => array(	// Покрытие - глянцевый/матовый
				"value" => rand(1, 2),
				"options" => array(
					array(
						"value" => 1,
						"name" => 'Матовый'
					),
					array(
						"value" => 2,
						"name" => 'Глянцевый'
					),
				)
			),
			"color" => array(	// цвет
				"value" => rand(1, 8),
				"options" => array(
					array(
						"value" => 1,
						"name" => "Сауле",
						"image" => "http://sill.maestros.ru/public/img/filter/saule.png",
					),
					array(
						"value" => 2,
						"name" => "Антрацит",
						"image" => "http://sill.maestros.ru/public/img/filter/antracit.png",
					),
					array(
						"value" => 3,
						"name" => "Серебристый ясень",
						"image" => "http://sill.maestros.ru/public/img/filter/2525.png",
					),
					array(
						"value" => 4,
						"name" => "Пазл",
						"image" => "http://sill.maestros.ru/public/img/filter/pazl.png",
					),
					array(
						"value" => 5,
						"name" => "Серо-бежевый мрамор",
						"image" => "http://sill.maestros.ru/public/img/filter/bristol.jpg"
					),
					array(
						"value" => 6,
						"name" => "Оранжевый",
						"image" => "http://sill.maestros.ru/public/img/filter/oranzh.jpg"
					),
					array(
						"value" => 7,
						"name" => "Бежевый мрамор",
						"image" => "http://sill.maestros.ru/public/img/filter/palermo.jpg"
					),
					array(
						"value" => 8,
						"name" => "Серый мрамор",
						"image" => "http://sill.maestros.ru/public/img/filter/mram.jpg"
					),
				)
			),
			"brand" => array(	// бренд
				"value" => rand(1, 4),
				"options" => array(
					array(
						"value" => 1,
						"name" => "CRYSTALLIT",
					),
					array(
						"value" => 2,
						"name" => "DANKE",
					),
					array(
						"value" => 3,
						"name" => "ESTERA",
					),
					array(
						"value" => 4,
						"name" => "MOELLER",
					),
				),
			),
			"quantity" => array(	// количество
				"value" => 1,
			),
			"width" => array(	// ширина
				"value" => '',
				"options" => array(
					array(
						"value" => 10,
						"name" => '10 см',
						"priceRatio" => 0.4
					),
					array(
						"value" => 15,
						"name" => '15 см',
						"priceRatio" => 0.5
					),
					array(
						"value" => 20,
						"name" => '20 см',
						"priceRatio" => 0.6
					),
					array(
						"value" => 25,
						"name" => '25 см',
						"priceRatio" => 0.7
					),
					array(
						"value" => 30,
						"name" => '30 см',
						"priceRatio" => 0.8
					),
					array(
						"value" => 35,
						"name" => '35 см',
						"priceRatio" => 0.9
					),
					array(
						"value" => 40,
						"name" => '40 см',
						"priceRatio" => 1
					),
					array(
						"value" => 45,
						"name" => '45 см',
						"priceRatio" => 1.1
					),
					array(
						"value" => 50,
						"name" => '50 см',
						"priceRatio" => 1.2
					),
					array(
						"value" => 55,
						"name" => '55 см',
						"priceRatio" => 1.3
					),
					array(
						"value" => 60,
						"name" => '60 см',
						"priceRatio" => 1.4
					)
				),
			)
		);

		$data['info'] = array(
			array(
				"title" => "Описание",
				"type" => "tabs",
				"content" => array(
					array(
						"title" => 'Описание',
						"text" => '<p>При оформлении заказа выберите Онлайн банковской картой и нажмите Перейти к оплате онлайн. Введите данные карты и нажмите Оплатить. Минимальная сумма оплаты — 1 рубль.</p><p>К оплате принимаются банковские карты, у которых 16, 18, 19 цифр в номере:</p><ul><li>VISA, MasterCard, American Express;</li><li>VISA Electron/Plus, Cirrus/Maestro, если у них есть код CVC2 и CVV2;</li><li>МИР;</li><li>Халва, Совесть, Хоум Кредит.</li></ul><p>После оплаты заказа вам на почту придет электронный чек. Все электронные чеки хранятся в личном кабинете в разделе Электронные чеки.</p>',
					),
					array(
						"title" => 'Характеристики',
						"text" => '<p>Производитель: <span class="color">Crystallit</span></p><p>Цвет: <span class="color">Орех</span></p><p>Декоры: <span class="color">Под дерево</span></p><p>Материал: <span class="color">ПВХ</span></p><p>Степень блеска: <span class="color">Глянцевый</span></p>',
					),
				)
			),

			array(
				"title" => "Фотогалерея",
				"type" => "gallery",
				"content" => array(
					'http://placekitten.com/g/1200/900',
					'http://placekitten.com/g/1100/500',
					'http://placekitten.com/g/800/800',
				)
			),

			array(
				"title" => "Дополнительно",
				"type" => "text",
				"content" => '<h3>Оплата по безналичному расчету</h3><ul><li>заключение долгосрочных и разовых договоров;</li><li>предоставление полного комплекта документов;</li><li>обслуживание квалифицированным менеджером;</li><li>доставку на следующий день после поступления денежных средств на расчетный счет продавца;</li><li>организацию доставки в регионы;</li><li>информационную поддержку постоянных клиентов;</li><li>индивидуальный подход к каждому клиенту.</li></ul><h3>Характеристики</h3><p>Производитель: <span class="color">Crystallit</span></p><p>Цвет: <span class="color">Орех</span></p><p>Декоры: <span class="color">Под дерево</span></p><p>Материал: <span class="color">ПВХ</span></p><p>Степень блеска: <span class="color">Глянцевый</span></p><p><img src="http://placekitten.com/g/800/600"></p><blockquote>Этот параметр также показывает количество материла, используемое при производстве ПД. Стоит обратить внимание, что заявленные параметры не соответствуют реалиям. Подоконник Меллер отличается на 0,2 мм, Кристаллит на 0,9 мм, Данке не заявляет официально толщину, наверное, потому, что она самая наименьшая.</blockquote>'
			),

		);


		$data['related'] = array();

		for ($i = 1; $i <= 5; $i++) {

			array_push($data['related'], array(
				"id" => $i,
				"collection" => getRandomCollection(),
				"name" => getRandomColor() . " " . getRandomMaterial(),
				"image" => "http://sill.maestros.ru/img/prod.png",
				"price" => rand(1, 10) * 100,
				"dimension" => "м. пог",
				"in_favourite" => rand(0, 4) == 1,
				"in_cart" => rand(0, 4) == 1,
			));

		}



		break;

	case 'get_compare_list':

		$data['status'] = true;

		$data['compare'] = array( // список сравниваемых продуктов

			"product" => array(
				"title" => "Сравнение подоконников",
				"items" => array(
					array(
						"id" => 1,
						"image" => "http://sill.maestros.ru/img/prod.png",
						"name" => "Crystallit"
					),
					array(
						"id" => 3,
						"image" => "http://sill.maestros.ru/img/prod.png",
						"name" => "Estera"
					),
					array(
						"id" => 8,
						"image" => "http://sill.maestros.ru/img/prod.png",
						"name" => "Danke"
					),
					array(
						"id" => 5,
						"image" => "http://sill.maestros.ru/img/prod.png",
						"name" => "Möller"
					),
					array(
						"id" => 10,
						"image" => "http://sill.maestros.ru/img/prod.png",
						"name" => "Möller"
					),
				)
			),

			"price" => array(
				"title" => "Стоимость, за 1 м. пог.",
				"items" => array(
					array(
						"text" => "600 руб.",
					),
					array(
						"text" => "600 руб.",
					),
					array(
						"text" => "600 руб.",
					),
					array(
						"text" => "600 руб.",
					),
					array(
						"text" => "600 руб.",
					),
				)
			),

			"props" => array(
				array(
					"title" => "Описание",
					"items" => array(
						array(
							"text" => "<p>Подоконники Кристаллит.</p><p>ООО «Кристаллит» входит в ГК «Витраж», российского лидера на рынке изделий из ПВХ. Сфера деятельности компании - продажа подоконников торговой марки Crystalit. Производственно-складской комплекс ООО «Кристаллит» расположен в Московской области, Солнечногорский район д. Дурыкино. Подоконная доска Crystalit прошла российскую и европейскую сертификацию, отвечает всем нормам безопасности и экологичности. Производство полностью автоматизировано оборудованием известнейших европейских фирм «Technoplast», «Krauss Maffei», «MHF». Полная ориентация производства на выпуск подоконников гарантирует исполнение заказа необходимого количества и качества точно в срок, без задержек.</p>",
						),
						array(
							"text" => "<p>Подоконники Кристаллит.</p><p>ООО «Кристаллит» входит в ГК «Витраж», российского лидера на рынке изделий из ПВХ. Сфера деятельности компании - продажа подоконников торговой марки Crystalit. Производственно-складской комплекс ООО «Кристаллит» расположен в Московской области, Солнечногорский район д. Дурыкино. Подоконная доска Crystalit прошла российскую и европейскую сертификацию, отвечает всем нормам безопасности и экологичности. Производство полностью автоматизировано оборудованием известнейших европейских фирм «Technoplast», «Krauss Maffei», «MHF». Полная ориентация производства на выпуск подоконников гарантирует исполнение заказа необходимого количества и качества точно в срок, без задержек.</p>",
						),
						array(
							"text" => "<p>Подоконники Кристаллит.</p><p>ООО «Кристаллит» входит в ГК «Витраж», российского лидера на рынке изделий из ПВХ. Сфера деятельности компании - продажа подоконников торговой марки Crystalit. Производственно-складской комплекс ООО «Кристаллит» расположен в Московской области, Солнечногорский район д. Дурыкино. Подоконная доска Crystalit прошла российскую и европейскую сертификацию, отвечает всем нормам безопасности и экологичности. Производство полностью автоматизировано оборудованием известнейших европейских фирм «Technoplast», «Krauss Maffei», «MHF». Полная ориентация производства на выпуск подоконников гарантирует исполнение заказа необходимого количества и качества точно в срок, без задержек.</p>",
						),
						array(
							"text" => "<p>Подоконники Кристаллит.</p><p>ООО «Кристаллит» входит в ГК «Витраж», российского лидера на рынке изделий из ПВХ. Сфера деятельности компании - продажа подоконников торговой марки Crystalit. Производственно-складской комплекс ООО «Кристаллит» расположен в Московской области, Солнечногорский район д. Дурыкино. Подоконная доска Crystalit прошла российскую и европейскую сертификацию, отвечает всем нормам безопасности и экологичности. Производство полностью автоматизировано оборудованием известнейших европейских фирм «Technoplast», «Krauss Maffei», «MHF». Полная ориентация производства на выпуск подоконников гарантирует исполнение заказа необходимого количества и качества точно в срок, без задержек.</p>",
						),
						array(
							"text" => "<p>Подоконники Кристаллит.</p><p>ООО «Кристаллит» входит в ГК «Витраж», российского лидера на рынке изделий из ПВХ. Сфера деятельности компании - продажа подоконников торговой марки Crystalit. Производственно-складской комплекс ООО «Кристаллит» расположен в Московской области, Солнечногорский район д. Дурыкино. Подоконная доска Crystalit прошла российскую и европейскую сертификацию, отвечает всем нормам безопасности и экологичности. Производство полностью автоматизировано оборудованием известнейших европейских фирм «Technoplast», «Krauss Maffei», «MHF». Полная ориентация производства на выпуск подоконников гарантирует исполнение заказа необходимого количества и качества точно в срок, без задержек.</p>",
						),
					)
				),
				array(
					"title" => "Толщина горизонтальных стенок, мм",
					"items" => array(
						array(
							"text" => "3",
						),
						array(
							"text" => "3",
						),
						array(
							"text" => "Не указано",
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
				array(
					"title" => "Толщина вертикальных стенок, мм",
					"items" => array(
						array(
							"text" => "1.2",
						),
						array(
							"text" => "2.2",
						),
						array(
							"text" => "Не указано",
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
				array(
					"title" => "Высота капиноса, мм",
					"items" => array(
						array(
							"text" => "40",
						),
						array(
							"text" => "30",
						),
						array(
							"text" => "Не указано",
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
				array(
					"title" => "Масса подоконника, Кг/м²",
					"items" => array(
						array(
							"text" => "Не указано",
						),
						array(
							"text" => "10",
						),
						array(
							"text" => "Не указано",
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
				array(
					"title" => "Количество ПВХ в составе, %",
					"items" => array(
						array(
							"text" => "Не указано",
						),
						array(
							"text" => "70",
						),
						array(
							"text" => "Не указано",
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
				array(
					"title" => "Устойчивость к царапанью, N",
					"items" => array(
						array(
							"text" => "Высокая устойчивость",
						),
						array(
							"text" => "5",
						),
						array(
							"text" => "Высокая устойчивость",
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
				array(
					"title" => "Устойчивость к химикатам, степень",
					"items" => array(
						array(
							"text" => "Устойчив к пятнам",
						),
						array(
							"text" => "5",
						),
						array(
							"text" => "Стойкость к химии",
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
				array(
					"title" => "Прочность на изгиб, кг",
					"items" => array(
						array(
							"text" => "Не указано",
						),
						array(
							"text" => "428",
						),
						array(
							"text" => "Не указано",
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
				array(
					"title" => "Толщина подоконника, мм",
					"items" => array(
						array(
							"text" => "20.30",
							"images" => array(
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "22.03",
							"images" => array(
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "18.30",
							"images" => array(
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
				array(
					"title" => "Толщина горизонтальной верхней стенки, мм",
					"description" => "Этот параметр также показывает количество материла, используемое при производстве ПД. Стоит обратить внимание, что заявленные параметры не соответствуют реалиям. Подоконник Меллер отличается на 0,2 мм, Кристаллит на 0,9 мм, Данке не заявляет официально толщину, наверное, потому, что она самая наименьшая."
				),
				array(
					"title" => "",
					"items" => array(
						array(
							"text" => "2.91",
							"images" => array(
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "2.8",
							"images" => array(
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "2.17",
							"images" => array(
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
				array(
					"title" => "Толщина вертикальных стенок, мм",
					"description" => "В этом тесте Меллер уступил заявленным параметрам на целых 0,6 мм (было заявлена толщина 2,2 мм, по факту максимальная толщина получилась 1.57мм). Подоконник Кристаллит превысил в этом тесте заявленные параметры."
				),
				array(
					"title" => "",
					"items" => array(
						array(
							"text" => "Подоконник Кристаллит имеет толщину ребер жесткости 1,68 до 1.73 мм. Ребра жесткости имеют бочкообразную форму, и ближе к стенкам подоконника толщина уменьшается до 1.3 мм.",
							"images" => array(
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "Подоконник Меллер имеет толщину ребер жесткости от 1,12 до 1,57 мм.",
							"images" => array(
								'http://placekitten.com/g/300/200',
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "Подоконник Данке имеет толщину ребер жесткости 0.78 до 1.07 мм.",
							"images" => array(
								'http://placekitten.com/g/300/200',
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
				array(
					"title" => "Масса подоконника, грамм",
					"description" => "Очень интересный параметр, как масса. Чем меньше весит подоконник, тем меньше плотность сырья при одинаковой геометрии профиля, тем меньше его используется. Структура пластмассы подоконника кристаллит и Данке очень похожа и, скорее всего, она сделана из чистого ПВХ с добавлением мела. Его количество мы постараемся определить ниже. В подоконник Меллер, как написано в интернете во многих источниках, добавлена древесная мука, которая, как говорит производитель, повышает экологичность продукта. На самом деле древесная мука является хорошим и недорогим наполнителем для пластмассы, готовый продукт имеет низкую плотность и соответственно маленьких вес и маленький расход сырья на квадратный метр продукции. Древесную муку можно увидеть в составе подоконника"
				),
				array(
					"title" => "",
					"items" => array(
						array(
							"text" => "266",
							"images" => array(
								'http://placekitten.com/g/300/200',
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "250",
							"images" => array(
								'http://placekitten.com/g/300/200',
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "226",
							"images" => array(
								'http://placekitten.com/g/300/200',
								'http://placekitten.com/g/300/200',
							)
						),
						array(
							"text" => "-",
						),
						array(
							"text" => "-",
						),
					)
				),
			),

		);


		break;

	case 'get_favourite_list':

		$data['status'] = true;

		$data['list'] = array();

		for ($i = 1; $i <= 18; $i++) {

			array_push($data['list'], array(
				"id" => $i,
				"collection" => getRandomCollection(),
				"name" => getRandomColor() . " " . getRandomMaterial(),
				"image" => "http://sill.maestros.ru/img/prod.png",
				"price" => rand(1, 10) * 100,
				"dimension" => "м. пог",
				"in_favourite" => true,
				"in_cart" => rand(0, 4) == 1,
			));

		}

		break;

	case 'get_filter':

		$data['status'] = true;

		$data['filters'] = array(
			array(
				"title" => "Бренд",
				"list" => array(
					array(
						"id" => "filter_1",
						"name" => "CRYSTALLIT",
						"info" => "<p>DANKE — торговая марка украинской производственной компании «Юпитер-2003». Это стабильно развивающееся предприятие, которое завоевало репутацию надежного производителя рынка строительных и … отделочных материалов. Производство ПВХ подоконников осуществляется на высокотехнологичном оборудовании (производство - Германия)</p><p><a href='/' target='_blank'>На страницу бренда</a></p></p>",
					),
					array(
						"id" => "filter_2",
						"name" => "DANKE",
					),
					array(
						"id" => "filter_3",
						"name" => "ESTERA",
					),
					array(
						"id" => "filter_4",
						"name" => "MOELLER",
					),
				)
			),
			array(
				"title" => "Покрытие",
				"list" => array(
					array(
						"id" => "filter_5",
						"name" => "Матовый"
					),
					array(
						"id" => "filter_6",
						"name" => "Глянцевый",
					),
				)
			),
			array(
				"title" => "Цвет",
				"list" => array(
					array(
						"id" => "filter_7",
						"name" => "Глория",
						"image" => "http://sill.maestros.ru/public/img/filter/gloria.png"
					),
					array(
						"id" => "filter_8",
						"name" => "Метелик",
						"image" => "http://sill.maestros.ru/public/img/filter/metelik.jpg"
					),
					array(
						"id" => "filter_9",
						"name" => "Вилья",
						"image" => "http://sill.maestros.ru/public/img/filter/vilia.jpg"
					),
					array(
						"id" => "filter_10",
						"name" => "Сауле",
						"image" => "http://sill.maestros.ru/public/img/filter/saule.png"
					),
					array(
						"id" => "filter_11",
						"name" => "Антрацит",
						"image" => "http://sill.maestros.ru/public/img/filter/antracit.png"
					),
					array(
						"id" => "filter_12",
						"name" => "Серебристый ясень",
						"image" => "http://sill.maestros.ru/public/img/filter/2525.png"
					),
					array(
						"id" => "filter_13",
						"name" => "Пазл",
						"image" => "http://sill.maestros.ru/public/img/filter/pazl.png"
					),
					array(
						"id" => "filter_14",
						"name" => "Серо-бежевый мрамор",
						"image" => "http://sill.maestros.ru/public/img/filter/bristol.jpg"
					),
					array(
						"id" => "filter_15",
						"name" => "Оранжевый",
						"image" => "http://sill.maestros.ru/public/img/filter/oranzh.jpg"
					),
					array(
						"id" => "filter_16",
						"name" => "Бежевый мрамор",
						"image" => "http://sill.maestros.ru/public/img/filter/palermo.jpg"
					),
					array(
						"id" => "filter_17",
						"name" => "Серый мрамор",
						"image" => "http://sill.maestros.ru/public/img/filter/mram.jpg"
					),
					array(
						"id" => "filter_18",
						"name" => "Коричневый дуб",
						"image" => "http://sill.maestros.ru/public/img/filter/korichd.png"
					),
					array(
						"id" => "filter_19",
						"name" => "Венге",
						"image" => "http://sill.maestros.ru/public/img/filter/venge.jpg"
					),
					array(
						"id" => "filter_20",
						"name" => "Белое дерево",
						"image" => "http://sill.maestros.ru/public/img/filter/bely_dub.jpg"
					),
					array(
						"id" => "filter_21",
						"name" => "Горная лиственница",
						"image" => "http://sill.maestros.ru/public/img/filter/gornlist.png"
					),
					array(
						"id" => "filter_22",
						"name" => "Дуб янтарный",
						"image" => "http://sill.maestros.ru/public/img/filter/oak-amber.png"
					),
					array(
						"id" => "filter_23",
						"name" => "Орех",
						"image" => "http://sill.maestros.ru/public/img/filter/oreh_glian.jpg"
					),
					array(
						"id" => "filter_24",
						"name" => "Золотой дуб",
						"image" => "http://sill.maestros.ru/public/img/filter/oak-gold.png"
					),
					array(
						"id" => "filter_25",
						"name" => "Дуб натуральный",
						"image" => "http://sill.maestros.ru/public/img/filter/oak-natural.png"
					),
					array(
						"id" => "filter_26",
						"name" => "Белый",
						"image" => "http://sill.maestros.ru/public/img/filter/white.png"
					),
					array(
						"id" => "filter_27",
						"name" => "Махагон",
						"image" => "http://sill.maestros.ru/public/img/filter/oak-amber.png"
					),
					array(
						"id" => "filter_28",
						"name" => "Розовый оникс",
						"image" => "http://sill.maestros.ru/public/img/filter/rozon.png"
					),

				)
			),
		);


		break;

	case 'get_order_list':

		$data['status'] = true;

		$data['list'] = array();

		$data['discount'] = $discount;

		for ($i = 1; $i <= $in_cart; $i++) {

			$price = rand(3, 10) * 100;

			array_push($data['list'], array(
				"id" => $i,
				"collection" => getRandomCollection(),
				"name" => getRandomColor() . " " . getRandomMaterial(),
				"image" => "http://sill.maestros.ru/img/prod.png",
				"price" => $price,
				"dimension" => "м. пог",
				"conditions" => array(
					"lgth" => array(		// длина
						"value" => rand(1, 3) * 100,
						"ones" => 100,		// целая единица длины (длина по которой расчитывается цена на единицу изделия - 100 cм. = 1 единица = 700 руб.)
						"maxValue" => 600
					),
					"quantity" => array(	// количество
						"value" => rand(1, 10),
					),
					"width" => array(	// ширина
						"value" => rand(2, 12) * 5,
						"options" => array(

							array(
								"value" => 10,
								"priceRatio" => 0.4
							),
							array(
								"value" => 15,
								"priceRatio" => 0.5
							),
							array(
								"value" => 20,
								"priceRatio" => 0.6
							),
							array(
								"value" => 25,
								"priceRatio" => 0.7
							),
							array(
								"value" => 30,
								"priceRatio" => 0.8
							),
							array(
								"value" => 35,
								"priceRatio" => 0.9
							),
							array(
								"value" => 40,
								"priceRatio" => 1
							),
							array(
								"value" => 45,
								"priceRatio" => 1.1
							),
							array(
								"value" => 50,
								"priceRatio" => 1.2
							),
							array(
								"value" => 55,
								"priceRatio" => 1.3
							),
							array(
								"value" => 60,
								"priceRatio" => 1.4
							)
						),
					)
				)
			));

		}

		break;

	case 'remove_order_item':

		$data['status'] = true;

		break;
	case 'add_order_item':

		$data['status'] = true;

		break;
	case 'add_favourite_item':

		$data['status'] = true;

		break;
	case 'remove_favourite_item':

		$data['status'] = true;

		break;
	case 'add_to_compare':

		$data['status'] = true;

		break;
	case 'remove_from_compare':

		$data['status'] = true;

		break;
	case 'submit_order':

		$data['status'] = true;
		$data['info_popup'] = array(
			"title" => "Подтверждение покупки",
			"message" => "Спасибо за заказ, после оплаты наш менеджер свяжется с вами. Детали заказа будут отправлены вам на почту."
		);

		break;
	case 'get_recall':

		$data['status'] = true;
		$data['info_popup'] = array(
			"message" => "Спасибо за заявку, скоро перезвоним."
		);

		break;
}


echo json_encode($data);
?>