import Popup from './popup'
import Field from './form'
import Icon from './icon'
import SwitchItem from './element/switch'
import TabWrap from './tabs/tab-wrap'
import TabLink from './tabs/tab-link'
import TabItem from './tabs/tab-item'
import Gallery from './gallery'
import GalleryWrap from './gallery-wrap'
import PageWrap from './page-wrap'
import ScrollBar from './scrollbar'
import FormWrap from './form/form-wrap'


export {
	Popup,
	Field,
	Icon,
	SwitchItem,
	FormWrap,
	TabWrap,
	TabLink,
	TabItem,
	Gallery,
	GalleryWrap,
	PageWrap,
	ScrollBar,
}