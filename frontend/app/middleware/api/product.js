import axios from './instance'

export default {

	list(filter) {
		return axios.post('', {action: 'get_product_list', ...filter}).then(r => r.data)
	},

	item({id}) {
		return axios.post('', {action: 'get_product_item', id}).then(r => r.data)
	},

	filter() {
		return axios.post('', {action: 'get_filter'}).then(r => r.data)
	},


}