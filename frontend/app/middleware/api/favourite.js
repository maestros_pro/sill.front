import axios from './instance'

export default {

	list() {
		return axios.post('', {action: 'get_favourite_list'}).then(r => r.data)
	},

	add(id) {
		return axios.post('', {action: 'add_favourite_item', id}).then(r => r.data)
	},

	remove(id) {
		return axios.post('', {action: 'remove_favourite_item', id}).then(r => r.data)
	},


}