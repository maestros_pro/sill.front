import axios from './instance'

export default {

	list() {
		return axios.post('', {action: 'get_compare_list'}).then(r => r.data)
	},

	add(id) {
		console.info(id);
		return axios.post('', {action: 'add_to_compare', id}).then(r => r.data)
	},

	remove(id) {
		return axios.post('', {action: 'remove_from_compare', id}).then(r => r.data)
	},


}