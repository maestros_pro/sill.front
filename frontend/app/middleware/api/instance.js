import axios from 'axios'
import eventHub from "../library/eventhub";
import Vue from 'vue'

axios.defaults.baseURL = isLocal || isDev ? 'https://sill.maestros.ru/api.php' : '/api.php';

axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';

axios.defaults.transformRequest = [function (data) {

	let formData = new FormData();

	Object.keys(data).map(function(key) {
		if (Array.isArray(data[key])) {

			for (let i = 0; i < data[key].length; i++) {
				formData.append(`${key}[]`, typeof data[key][i] === 'object' ?  JSON.stringify(data[key][i]) : data[key][i]);
			}

		} else {
			formData.append(key, data[key]);
		}
	});

	return formData;
}];



let countRequest = 0;

axios.interceptors.request.use(config => {
	startLoading();
	return config;
}, error => {
	finishLoading();
	return Promise.reject(error);
});

axios.interceptors.response.use(
	response => {
		//console.info(response);
		finishLoading();
		return response;
	},
	error => {
		finishLoading();
		return Promise.reject(error);
	}
);

function startLoading() {
	if ( countRequest <= 0) eventHub.$emit('start-loading');
	countRequest++;
}

function finishLoading() {
	countRequest--;
	if ( countRequest <= 0 ) {
		countRequest = 0;
		eventHub.$emit('finish-loading');
	}
}

export default axios;
