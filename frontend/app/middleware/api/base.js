import axios from './instance'

export default {

	commonInfo() {
		return axios.post('', {action: 'get_common_info'}).then(r => r.data)
	},

	getRecall(phone){
		return axios.post('', {action: 'get_recall', phone}).then(r => r.data)
	}

}