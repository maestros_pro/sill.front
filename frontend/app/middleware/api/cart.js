import axios from './instance'

export default {

	list() {
		return axios.post('', {action: 'get_order_list'}).then(r => r.data)
	},

	add(id, data) {
		console.info(id, data);
		return axios.post('', {action: 'add_order_item', id, ...data}).then(r => r.data)
	},

	remove(id) {
		return axios.post('', {action: 'remove_order_item', id}).then(r => r.data)
	},

	submit(data) {
		return axios.post('', {action: 'submit_order', ...data}).then(r => r.data)
	},


}