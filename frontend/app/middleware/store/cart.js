import {cart as Api} from '../api'

export default {
	namespaced: true,

	state: {
		discount: 0,
		price: 0,
		list: [],

		orderData: {
			name: '',
			phone: '',
			email: '',
			address: '',
			payment: '',
		},

		userAddress: {
			user_address_street: '',
			user_address_house: '',
			user_address_part: '',
			user_address_room: '',
			user_address_floor: '',
			user_address_comment: '',
			user_address_lit: '',
			user_address: '',
			user_address_postal: '',
			user_address_country: '',
			user_address_city: '',
		}
	},

	mutations: {

		SET_CART(state, {list, discount}){

			let obj = [];

			for(let i = 0; i < list.length; i++){
				let item = list[i];
				item.loading = false;
				item.priceRatio = 1;
				item.model = {};
				item.model.width = item.conditions.width.value;
				item.model.lgth = item.conditions.lgth.value;
				item.model.quantity = item.conditions.quantity.value;
				item.cost = 0;
				obj.push(item);
			}

			state.list = [...obj];
			state.discount = discount || 0;

		},

		CALC_PRICE(state){
			let price = 0;
			for(let i = 0; i < state.list.length; i++){
				const item = state.list[i];
				let itemOption = item.conditions.width.options.find( itm => itm.value === item.model.width);
				state.list[i].priceRatio = itemOption ? itemOption.priceRatio : 1;
				state.list[i].cost = parseInt(state.list[i].price * state.list[i].model.lgth / state.list[i].conditions.lgth.ones * state.list[i].model.quantity * state.list[i].priceRatio);
				price += state.list[i].cost || 0;
			}
			state.price = parseInt(price);
		},

		SET_ITEM_PROP(state, {id, name, value}){
			let idx = state.list.findIndex(i => i.id === id);
			state.list[idx].model[name] = value;
		},

		SET_ADDRESS_PROP(state, {id, name, value}){
			state.userAddress[name] = value;
		},

		CLEAR_ADDRESS_PROP(state){
			state.userAddress = JSON.parse(JSON.stringify({
				user_address_street: '',
				user_address_house: '',
				user_address_part: '',
				user_address_room: '',
				user_address_floor: '',
				user_address_comment: '',
				user_address_lit: '',
				user_address: '',
				user_address_postal: '',
				user_address_country: '',
				user_address_city: '',
			}));
		},

		SET_ORDER_PROP(state, {name, value}){
			state.orderData[name] = value;
		},
	},

	actions: {

		list({commit}){

			return Api.list()
				.then((res)=>{
					commit('SET_CART', res);
					commit('CALC_PRICE');
				})
			;
		},

		remove({state, commit}, id){

			let idx = state.list.findIndex(i => i.id === id);
			state.list[idx].loading = true;

			return Api.remove(id)
				.then(()=>{
					let idx = state.list.findIndex(i => i.id === id);
					state.list = [...state.list.slice(0, idx), ...state.list.slice(idx + 1)];
					commit('CALC_PRICE');
				})
				.catch(()=>{
					let idx = state.list.findIndex(i => i.id === id);
					state.list[idx].loading = false;
				})
			;
		},


		submit({state}){
			let data = Object.assign({}, state.orderData, state.userAddress);
			return Api.submit(data);
		},


	},

	getters: {
		allowOrder: state => {
			let courier = state.orderData.address === 'courier' ? state.userAddress.user_address_street && state.userAddress.user_address_house && state.userAddress.user_address_room: true;
			return !!(
				courier &&
				state.orderData.name &&
				state.orderData.phone &&
				state.orderData.email &&
				state.orderData.address &&
				state.orderData.payment
			);
		},

		getUserAddress: state => {
			let address = `
			${state.userAddress.user_address_street} 
			, дом ${state.userAddress.user_address_house}
			${state.userAddress.user_address_part ? ', корп. ' + state.userAddress.user_address_part : ''}
			, кв. ${state.userAddress.user_address_room}
			${state.userAddress.user_address_floor ? ', этаж ' + state.userAddress.user_address_floor : ''}
			`;
			return state.userAddress.user_address_street
				&& state.userAddress.user_address_house
				&& state.userAddress.user_address_room ? address : false;
		}
	}
}