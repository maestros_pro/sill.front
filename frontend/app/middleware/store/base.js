import {base as Api} from '../api'

export default {
	namespaced: true,

	state: {
		showMenu: false,

		breakpoint: {
			mobile: false,
			tablet: false,
			desktop: false,
			current: ''
		},

		theme: 'dark',

		info: {
			in_cart: 0,
			in_favourite: 0,
		},

		showRecallPopup: true
	},

	mutations: {

		TOGGLE_MENU(state, value){
			state.showMenu = value !== undefined ? value : !state.showMenu;

			// if ( state.showMenu ){
			// 	document.body.style.overflow = 'hidden';
			// } else {
			// 	document.body.style.overflow = '';
			// }
		},

		SET_POPUP_RECALL(state, value){
			state.showRecallPopup = value
		},

		SET_THEME(state, theme){
			state.theme = theme
		},

		SET_INFO(state, {name, value}){
			state.info[name] = value;
		},

		SET_VIEWPORT(state, bp){
			Object.keys(state.breakpoint).map((key)=>{
				state.breakpoint[key] = key === bp;
			});
			state.breakpoint.current = bp;
		},

		UPDATE_INFO(state, info){
			Object.keys(info).map((key)=>{
				state.info[key] = info[key];
			});
		},
	},

	actions: {

		commonInfo(context){

			return Api.commonInfo()
				.then((res)=>{
					context.commit('UPDATE_INFO', res.info);
				})
			;
		},

		getRecall(context, phone){
			return Api.getRecall(phone);
		},

	},

	getters: {

	}
}