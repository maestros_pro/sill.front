
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

import base from './base'
import cart from './cart'
import product from './product'
import compare from './compare'

export default new Vuex.Store({
	modules: {
		base,
		product,
		compare,
		cart,
	},

})


