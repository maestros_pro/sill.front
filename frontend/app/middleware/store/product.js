import {product as Api, favourite as Fav, cart as Cart} from '../api'

const _defaultItemOptions = {
	id: null,
	collection: null,
	name: null,
	images: [],
	price: 0,
	dimension: '',
	in_favourite: '',
};

const _defaultItemModel= {
	lgth: '',
	surface: '',
	color: '',
	brand: '',
	quantity: 1,
	width: '',
};


export default {
	namespaced: true,

	state: {
		filters: [],

		view: 'grid',

		list: [],

		item: {
			data: {},
			model: {},
			form: {},
			info: [],
		},

		filterModel: []
	},

	mutations: {

		SET_PRODUCT_LIST(state, value){
			state.list = [...value];
		},

		SET_DEFAULT_ITEM(state){
			state.item.data = Object.assign({}, _defaultItemOptions);
			state.item.model = Object.assign({}, _defaultItemModel);
		},

		SET_PRODUCT_ITEM(state, {item, form, info, related}){
			state.item.data = Object.assign({}, _defaultItemOptions, item);
			state.item.info = info ? [...info] : [];
			state.item.form = form;
			state.list = related ? [...related] : [];
			Object.keys(form).forEach( name => {
				state.item.model[name] = form[name].value;
			})
		},

		SET_FILTER_LIST(state, value){
			state.filters = [...value];
		},

		SET_ITEM_PROP(state, {name, value}){
			state.item.model[name] = value;
		},

		SET_ITEM_DATA_PROP(state, {name, value}){
			state.item.data[name] = value;
		},

		SET_VIEW(state, value){
			state.view = value;
		},

		SET_FILTER_MODEL(state, value){
			state.filterModel = value;
		},

		TOGGLE_FAVOURITE(state, id){
			let idx = state.list.findIndex( itm => itm.id === id );
			if (idx >= 0) state.list[idx].in_favourite = !state.list[idx].in_favourite;
		},

		TOGGLE_CART(state, id){
			let idx = state.list.findIndex( itm => itm.id === id );
			if (idx >= 0) state.list[idx].in_cart = !state.list[idx].in_cart;
		},

	},

	actions: {

		list({commit, state}){

			return Api.list({filter: state.filterModel})
				.then(({list})=>{
					commit('SET_PRODUCT_LIST', list);
				})
			;
		},

		async item({commit, state}, id){
			const value = await Api.item(id);
			commit('SET_PRODUCT_ITEM', value);
		},

		favourite({commit, state}){

			return Fav.list()
				.then(({list})=>{
					commit('SET_PRODUCT_LIST', list);
				})
			;
		},

		addToFavourite({commit, state}, id){
			return Fav.add(id)
				.then(()=>{
					commit('TOGGLE_FAVOURITE', id);
				})
			;
		},

		removeFromFavourite({commit, state}, id){
			return Fav.remove(id)
				.then(()=>{
					commit('TOGGLE_FAVOURITE', id);
				})
			;
		},

		addToCart({commit, state}, id){
			return Cart.add(id)
				.then(()=>{
					commit('TOGGLE_CART', id);
				})
			;
		},

		async addItemToCart({commit, state}){
			await Cart.add(state.item.data.id, state.item.model);
		},


		removeFromCart({commit, state}, id){
			return Cart.remove(id)
				.then(()=>{
					commit('TOGGLE_CART', id);
				})
			;
		},

		filter({commit}){

			return Api.filter()
				.then(({filters})=>{
					commit('SET_FILTER_LIST', filters);
				})
			;
		},

	},

	getters: {

	}
}