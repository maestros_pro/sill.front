import {compare as Api} from '../api'

export default {
	namespaced: true,

	state: {
		list: {
			product: {},
			price: {},
			props: [],
		}
	},

	mutations: {

		SET_COMPARE(state, {compare}){
			state.list = JSON.parse(JSON.stringify(compare))
		},
	},

	actions: {

		list({commit}){

			return Api.list()
				.then((res)=>{
					commit('SET_COMPARE', res);
				})
			;
		},

		add({state, commit}, id){
			return Api.add(id);
		},

		remove({state, commit}, id){
			return Api.remove(id);
		},


	},

	getters: {

	}
}