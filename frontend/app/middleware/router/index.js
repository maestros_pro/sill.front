import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

Vue.use(Router);

const router = new Router({
	//base: isProd ? '/' : '/',
	mode: isProd ? 'history' : 'hash',
	routes,
	linkActiveClass: '',
	linkExactActiveClass: 'is-active',
	transitionOnLoad: true,
	root: '/'
});

router.afterEach((to, from) => {
	document.title = to.meta.title;
});

export default router