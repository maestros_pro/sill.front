


export default [
	{
		path: '/',
		component: () => import('../../../app/views/home'),
		name: 'home',
		meta: {
			title: 'Главная'
		}
	},

	// catalog
	{
		path: '/catalog',
		component: () => import('../../views/catalog'),
		name: 'catalog-all',
		meta: {
			title: 'Каталог',
			group: 'catalog'
		}
	},
	{
		path: '/catalog/:id',
		component: () => import('../../views/catalog/item'),
		name: 'product',
		meta: {
			title: 'Продукт',
			group: 'product'
		}
	},

	{
		path: '/info',
		component: () => import('../../views/info'),
		name: 'info',
		meta: {
			title: 'О нас',
			group: 'about'
		},
	},

	// contacts
	{
		path: '/contacts',
		redirect: '/contacts/map'
	},
	{

		path: '/contacts/map',
		component: () => import('../../views/contacts/map'),
		name: 'contacts-map',
		meta: {
			title: 'Карта',
			group: 'contacts'
		},
	},
	{

		path: '/contacts/way',
		component: () => import('../../views/contacts/way'),
		name: 'contacts-way',
		meta: {
			title: 'Как проехать?',
			group: 'contacts'
		},
	},
	{
		path: '/contacts/mall',
		component: () => import('../../views/contacts/mall'),
		name: 'contacts-mall',
		meta: {
			title: 'Карта комплекса',
			group: 'contacts'
		},
	},


	// compare
	{
		path: '/compare',
		component: () => import('../../views/compare'),
		name: 'compare-list',
		meta: {
			title: 'Сравнение',
			group: 'compare'
		},
	},
	// favourite
	{
		path: '/favourite',
		component: () => import('../../views/favourite'),
		name: 'favourite-list',
		meta: {
			title: 'Избранное',
			group: 'favourite'
		},
	},


	// cart
	{
		path: '/cart',
		redirect: '/cart/order'
	},
	{
		path: '/cart/order',
		component: () => import('../../views/cart/order'),
		name: 'cart-list',
		meta: {
			title: 'Состав заказа',
			group: 'cart'
		},
	},
	{
		path: '/cart/checkout',
		component: () => import('../../views/cart/checkout'),
		name: 'cart-checkout',
		meta: {
			title: 'Состав заказа',
			group: 'cart'
		},
	},



	{
		path: '*/*',
		component: () => import('../../../app/views/error.vue'),
		name: 'error',
		meta: {
			title: 'Страница не существует'
		}
	}
];