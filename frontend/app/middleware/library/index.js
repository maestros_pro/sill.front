import VTooltip from "v-tooltip";
import VueAwesomeSwiper from 'vue-awesome-swiper';
import lazyload from 'vue-lazyload'
import OverlayScrollbars from 'overlayscrollbars';
import VueDragDrop from 'vue-drag-drop';
import axios from '../api/instance'
import Snackbar from 'node-snackbar'
import VueDragscroll from 'vue-dragscroll'
import eventHub from "./eventhub";
import autosize from 'v-autosize/dist/plugin';
import YmapPlugin from 'vue-yandex-maps'

export default{

	VueAwesomeSwiper: {
		plugin: VueAwesomeSwiper,
		options: {}
	},

	autosize: {
		plugin: autosize,
		options: {}
	},

	Axios: {
		plugin: {
			install: (V)=>{
				V.prototype.$axios = axios;
			}
		},
		options: {}
	},

	YmapPlugin: {
		plugin: YmapPlugin,
		options: {
			apiKey: window.ymapsApiKey || 'fb22bfdd-4c00-4598-9a19-eba6ba33b360',
			lang: 'ru_RU',
			version: '2.1'
		}
	},

	eventHub: {
		plugin: {
			install: (V)=>{
				V.prototype.$eventHub = eventHub;
			}
		},
		options: {}
	},

	VueDragscroll: {
		plugin: VueDragscroll,
		options: {}
	},

	Snackbar: {
		plugin: {
			install: (V)=>{
				V.prototype.$snackbar = (opts)=>{
					let options = {
						text: opts.message,
						textColor: false,
						pos: '',
						customClass: opts.type || 'info',
						width: 'auto',
						showAction:	!!opts.event,
						actionText:	opts.event,
						actionTextAria:	'',
						alertScreenReader: false,
						actionTextColor: false,
						backgroundColor: false,
						duration: opts.duration || 6000,
						onActionClick: opts.action,
						onClose: ()=>{},
					};
					return Snackbar.show(options)
				};
			}
		},
		options: {}
	},

	VueDragDrop: {
		plugin: VueDragDrop,
		options: {}
	},

	OverlayScrollbars: {
		plugin: {
			install: (V)=>{
				V.directive('scrollbar', {
					inserted(el, binding, vnode) {
						let options = {
							className: 'os-theme-thin-light',
							sizeAutoCapable : true,
							paddingAbsolute : true,
							scrollbars : {
								clickScrolling : true,
								autoHide : "leave"
							},
							overflowBehavior:{
								x: 'hidden'
							}
						};
						if (!('ontouchstart' in window || navigator.msMaxTouchPoints) && !el.scrollbar) {
							el.scrollbar = OverlayScrollbars(el, Object.assign(options, binding.value));
						}
					},

					unbind(el, binding, vnode){
						if (el.scrollbar) el.scrollbar.destroy();
					}
				});
			}
		},
		options: {}
	},

	VTooltip: {
		plugin: VTooltip,
		options: {
			popover: {
				defaultWrapperClass: 'popover-wrapper',
				defaultBaseClass: 'popover',
				defaultInnerClass: 'popover-inner',
				defaultArrowClass: 'popover-arrow',
			}
		}
	},

	lazyload: {
		plugin: lazyload,
		options: {
			preLoad: 1.3,
			//error: require('../../../img/svg/btn-loader.svg'),
			loading: require('../../../img/svg/btn-loader.svg'),
			attempt: 1
		}
	},
};
