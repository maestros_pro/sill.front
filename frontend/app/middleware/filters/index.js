
export default {


	/**
	 *
	 * @param value - Number | String
	 * @param mark - String
	 * @returns {string}
	 *
	 * {{ '1000000000000' | discharge('*') }}
	 */
	discharge(value, mark = ' '){
		if (!value) return '';
		return String(value).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, `$1${mark}`);
	},


	/**
	 *
	 * @param value - Number
	 * @param args - String
	 * @returns {string|*}
	 *
	 * {{ 100001 | declension('день', 'дня', 'дней') }}
	 */
	declension(value, ...args){
		if (!value) return '';
		let cases = [2, 0, 1, 1, 1, 2],num = Number(value);
		if ( isNaN(num) ){
			console.error('declension must be a Number');
			return value;
		}
		else {
			return args[(num%100>4 && num%100<20)? 2 : cases[(num%10<5)?num%10:5]];
		}
	},

	/**
	 *
	 * @param value - String
	 * @param limit - Number
	 * @param postfix - String
	 * @param minLastLetter
	 * @returns {string|*}
	 *
	 * {{ asdasdasdasd | crop(10, '...') }}
	 */
	crop(value, limit, postfix = '...', minLastLetter = 2){
		if ( !value ) return '';
		if ( !limit ) return value;
		let text = value.toString().trim(), arr, lastSpace;
		if( text.length <= limit) return text;
		text = text.slice( 0, limit);
		arr = text.split(' ');
		lastSpace = text.lastIndexOf(' ');
		if ( arr[arr.length - 1].length < minLastLetter && lastSpace > 0 ){
			text = text.substr(0, lastSpace);
		}
		return text + postfix;
	}

}